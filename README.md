# labDataEvalVis

**labDataEvalVis** is a library of python routines for evaluating and visualisation of lab data. It offers many common convenient plots on top of plotting libraries like matplotlib, plotly, etc.